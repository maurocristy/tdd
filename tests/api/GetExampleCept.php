<?php
$I = new ApiTester($scenario);

$I->wantTo('I want to test a get example via API');
$I->sendGET('api/v1/example');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
$I->seeResponseIsJson();
$I->seeResponseContains('this is an example endpoint');
