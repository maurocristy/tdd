<?php

namespace Factories;

use Aivo\Factories\ProductFactory;
use Aivo\Entities\Product;

class ProductFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCreateSuccess()
    {
        $data = [
            'name' => 'pantalon',
            'description' => 'pantalon de prueba',
            'attributes' => [
                [
                    'name' => 'color',
                    'value' => 'azul'
                ],
                [
                    'name' => 'talle',
                    'value' => '34'
                ]
            ]
        ];
        
        $factory = new ProductFactory();
        $product = $factory->create($data);
        
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($data['name'], $product->getName());
        $this->assertEquals($data['description'], $product->getDescription());
        $this->assertCount(2, $product->getAttributes());
    }
}