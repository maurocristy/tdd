<?php

namespace Service;

use Codeception\Util\Stub;
use Doctrine\ORM\EntityManagerInterface;
use Aivo\Factories\ProductFactory;
use Aivo\Services\ProductService;
use Aivo\Entities\Product;
use Aivo\Entities\Category;

class UserServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCreateSuccess()
    {
        $service = new ProductService(
            $this->mockEntityManager(),
            new ProductFactory()
        );
        
        $data = [
            'name' => 'pantalon',
            'description' => 'pantalon de prueba',
            'attributes' => [
                [
                    'name' => 'color',
                    'value' => 'azul'
                ],
                [
                    'name' => 'talle',
                    'value' => '34'
                ]
            ]
        ];
        
        $category = $this->mockCategory();
        $user = $this->mockUser();
        
        $product = $service->create(
            $category,
            $user,
            $data
        );
        
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($data['name'], $product->getName());
        $this->assertEquals($data['description'], $product->getDescription());
        $this->assertCount(2, $product->getAttributes());
        $this->assertEquals($category, $product->getCategory());
        $this->assertEquals($user->id, $product->getUserId());
    }

    public function testUpdateSuccess()
    {
        $service = new ProductService(
            $this->mockEntityManager(),
            new ProductFactory()
        );
        
        $data = [
            'name' => 'pantalon 2',
            'description' => 'pantalon de prueba 2',
            'attributes' => [
                [
                    'name' => 'color',
                    'value' => 'verde'
                ]
            ]
        ];
        
        $product = $this->mockProduct();
        
        $service->update($product, $data);
        
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($data['name'], $product->getName());
        $this->assertEquals($data['description'], $product->getDescription());
        $this->assertCount(1, $product->getAttributes());
    }
    /**
     * 
     * @return Category
     */
    protected function mockCategory()
    {
        $category = Stub::makeEmpty(Category::class, [
            'id' => 1,
            'name' => 'Ropa',
            'slug' => 'ropa'
        ]);
        
        return $category;
    }
    
    /**
     * 
     * @return EntityManagerInterface
     */
    protected function mockEntityManager() {
        $em = Stub::makeEmpty(EntityManagerInterface::class);
            
        return $em;
    }
    
    /**
     * 
     * @return object
     */
    protected function mockUser()
    {
        $jsonUser = '{
            "id": "66",
            "username": "juan.perez",
            "email": "juan@perez.com",
            "name": "juan",
            "last_name": "perez",
            "birthdate": "1980-06-30"
          }';
        
        $user = json_decode($jsonUser);
        
        return $user;
    }
    
    protected function mockProduct()
    {
        $user = $this->mockUser();
        $category = $this->mockCategory();
        
        $factory = new ProductFactory();
        
        $data = [
            'name' => 'pantalon',
            'description' => 'pantalon de prueba',
            'attributes' => [
                [
                    'name' => 'color',
                    'value' => 'azul'
                ],
                [
                    'name' => 'talle',
                    'value' => '34'
                ]
            ]
        ];
        $product = $factory->create($data);
        
        $product->setCategory($category)
            ->setUserId($user->id);
        
        return $product;
        
    }
}