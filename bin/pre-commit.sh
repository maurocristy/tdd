#!/bin/sh

vendor/bin/phpcs;
vendor/bin/codecept run unit;
vendor/bin/codecept run api;
