# DDD & TDD Workshop #

Little API to show how DDD and TD works

## Install

Install using composer

``` bash
$ composer install
```

## Creating the virtual host

``` bash
# vhost.sh -d /var/www/<project>/public -s <project>.local.agentbot.net -p /var/www/ssl -c agentbot.net;
```

## Pre-commit hook
We use a pre-commit hook for git in order to know if we are breaking any unit tests. Because this script resides in the local .git folder, it needs to be copied manually. The script is provided in bin/pre-commit.sh and you can create a symlink to your .git/hooks/ folder. Assuming a default apache set up, you can simply run the following command, using the project root as your working directory.

```bash
$ ln -s /var/www/<project>/bin/pre-commit.sh /var/www/<project>/.git/hooks/pre-commit.sh
```

## Config

Create .env file with your local settings

``` bash
$ cp .env.example .env
```

## PHP built-in server

Run the following command in terminal to start localhost web server

``` bash
php -S localhost:8888 -t public public/index.php
```

## Database

Create the database and import this [dump.sql](/app/data/dump.sql)

## Tests

Run unit tests

``` bash
./vendor/bin/codecept run
```

## Domain diagram (class diagram)

[/app/docs/domain.png](/app/docs/domain.png)