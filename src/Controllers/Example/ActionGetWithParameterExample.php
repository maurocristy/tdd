<?php

namespace Aivo\Controllers\Example;

use Aivo\Controllers\BaseController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ActionGetWithParameterExample extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $parameterOne, $parameterTwo)
    {
        $data = [
            'info' => [sprintf('this is an example endpoint: %s, %s',
                $parameterOne,
                $parameterTwo
            )],
        ];

        $status = 200;

        return $this->withJson($response, $data, $status);
    }
}
