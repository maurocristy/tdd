<?php

namespace Aivo\Controllers\Users;

use Aivo\Controllers\BaseController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Aivo\Exceptions\BadRequestException;

class ActionGetUser extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $id)
    {
        $user = $this->getService('rest_user_repository')
            ->findById($id);
        
        return $this->withJson($response, $user, 200);
    }
}
