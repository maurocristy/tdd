<?php

namespace Aivo\Controllers\Status;

use Aivo\Controllers\BaseController;
use Slim\Http\Request;
use Slim\Http\Response;

class ActionException extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @throws \Aivo\Exceptions\NotFound
     */
    public function __invoke(Request $request, Response $response, $args = [])
    {
        throw new \Aivo\Exceptions\NotFound();
    }
}
