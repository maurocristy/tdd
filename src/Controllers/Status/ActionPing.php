<?php

namespace Aivo\Controllers\Status;

use Aivo\Controllers\BaseController;
use Slim\Http\Request;
use Slim\Http\Response;

class ActionPing extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $args = [])
    {
        $time = time();
        return $response->withJson(['status' => 'OK', 'time' => $time], 200);
    }
}
