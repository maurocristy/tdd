<?php

namespace Aivo\Controllers\Categories;

use Aivo\Controllers\BaseController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use Aivo\Entities\Category;

class ActionPostCategory extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response)
    {
        $category = new Category();
        $category->setName('test');
        
        $this->container->get('em')->persist($category);
        $this->container->get('em')->flush();
        
        return $this->withJson($response, [], 201);
    }
}
