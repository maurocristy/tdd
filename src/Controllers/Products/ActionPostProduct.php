<?php

namespace Aivo\Controllers\Products;

use Aivo\Controllers\BaseController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Aivo\Exceptions\BadRequestException;

class ActionPostProduct extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function __invoke(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $category = $this->findCategory($data['category']);
        $product = $this->getService('product_service')
            ->create($category, $data);
        
        return $this->withJson($response, $product, 201);
    }
    
    /**
     * 
     * @param string $slug
     * @return \Aivo\Entities\Category
     * @throws BadRequestException
     */
    protected function findCategory($slug)
    {
        $category = $this->getDoctrine()
            ->getRepository('Aivo\Entities\Category')
            ->findOneBy([
                'slug' => $slug
            ]);
        
        if (empty($category)) {
            $e = new BadRequestException();
            $e->setMessage('Invalid category');
            
            throw $e;
        }
        
        return $category;
    }
}
