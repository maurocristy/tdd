<?php

namespace Aivo\Controllers;

use Slim\Container;
use Slim\Http\Response;

class BaseController
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * BaseController constructor.
     * @param Container $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param $container
     * @return self
     */
    public function setContainer($container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @param Response $response
     * @param array    $data
     * @param int      $status
     *
     * @return Response
     */
    protected function withJson(Response $response, $data, $status = 200)
    {
        return $response->withJson($data, $status);
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    protected function getDoctrine()
    {
        return $this->container->get('em');
    }
    
    /**
     * 
     * @param string $service
     * @return mixed
     */
    protected function getService($service)
    {
        return $this->container->get($service);
    }
}
