<?php

namespace Aivo\Services;

use Doctrine\ORM\EntityManagerInterface;
use Aivo\Entities\Product;
use Aivo\Entities\Category;
use Aivo\Factories\ProductFactory;

class ProductService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    
    /**
     * @var ProductFactory 
     */
    protected $productFactory;
    
    /**
     * 
     * @param EntityManagerInterface $em
     * @param ProductFactory $productFactory
     */
    public function __construct(
        EntityManagerInterface $em,
        ProductFactory $productFactory
    ) {
        $this->em = $em;
        $this->productFactory = $productFactory;
    }
    
    /**
     * 
     * @param Category $category
     * @param array $data
     * @return Product
     */
    public function create(
        Category $category,
        array $data
    ) {
        $product = $this->productFactory->create($data);
        $product->setCategory($category);
        
        $this->em->persist($product);
        $this->em->flush();
        
        return $product;
    }
    
    public function update(
        Product $product,
        array $data
    ) {
        $product->setName($data['name'])
            ->setDescription($data['description']);
        
        $this->updateAttributes($product, $data['attributes']);
        
        $this->em->flush();
        
        return $product;
    }
    
    protected function updateAttributes(
        Product $product,
        array $attributesData
    ) {
        $product->removeAttributes();
        
        foreach ($attributesData as $attrData) {
            $attribute = $this->productFactory->createAttribute($attrData);
            $product->addAttribute($attribute);
            $attribute->setProduct($product);
        }
    }
}
