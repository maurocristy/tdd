<?php

namespace Aivo\Exceptions;

use Aivo\BaseException;
use Monolog\Logger;

class NotFound extends BaseException
{
    /**
     * @var string
     */
    public $message = 'Not found.';

    /**
     * @var string
     */
    public $level = self::WARNING;

    /**
     * @var int
     */
    public $httpCode = 404;

    /**
     * @var string
     */
    public $class = __CLASS__;
}
