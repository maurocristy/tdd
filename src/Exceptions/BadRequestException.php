<?php

namespace Aivo\Exceptions;

use Aivo\BaseException;
use Monolog\Logger;

class BadRequestException extends BaseException
{
    /**
     * @var string
     */
    public $message = 'Bad request.';

    /**
     * @var string
     */
    public $level = self::WARNING;

    /**
     * @var int
     */
    public $httpCode = 400;

    /**
     * @var string
     */
    public $class = __CLASS__;
}
