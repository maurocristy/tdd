<?php

namespace Aivo\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="attribute")
 */
class Attribute implements \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="attributes")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     **/
    protected $product;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $value;
    
    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * 
     * @return Product
     */
    function getProduct()
    {
        return $this->product;
    }

    /**
     * 
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @return string
     */
    function getValue()
    {
        return $this->value;
    }

    /**
     * 
     * @param Product $product
     * @return $this
     */
    function setProduct(Product $product)
    {
        $this->product = $product;
        
        return $this;
    }

    /**
     * 
     * @param string $name
     * @return $this
     */
    function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * 
     * @param string $value
     * @return $this
     */
    function setValue($value)
    {
        $this->value = $value;
        
        return $this;
    }

    public function jsonSerialize()
    {
        $attribute = [
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value
        ];
        
        return $attribute;
    }

}
