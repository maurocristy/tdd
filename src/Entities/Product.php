<?php

namespace Aivo\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Aivo\Repositories\ProductRepository")
 * @ORM\Table(name="product")
 * @ORM\HasLifecycleCallbacks
 */
class Product implements \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     **/
    protected $category;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    
    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt; 
    
    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Attribute", mappedBy="product", cascade={"persist", "remove"})
     **/
    protected $attributes;
    
    public function __construct() 
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->attributes = new ArrayCollection();
    }
    
    /** @ORM\PreUpdate */
    public function onPreUpdate() 
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * 
     * @return Category
     */
    function getCategory()
    {
        return $this->category;
    }

    /**
     * 
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @return string
     */
    function getDescription()
    {
        return $this->description;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * 
     * @return \DateTime
     */
    function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * 
     * @param Category $category
     * @return $this
     */
    function setCategory(Category $category)
    {
        $this->category = $category;
        
        return $this;
    }

    /**
     * 
     * @param string $name
     * @return $this
     */
    function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * 
     * @param string $description
     * @return $this
     */
    function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }

    /**
     * 
     * @param \DateTime $createdAt
     * @return $this
     */
    function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        
        return $this;
    }

    /**
     * 
     * @param \DateTime $updatedAt
     * @return $this
     */
    function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        
        return $this;
    }

    /**
     * 
     * @param \Aivo\Entities\Attribute $attribute
     * @return $this
     */
    public function addAttribute(Attribute $attribute)
    {
        $this->attributes->add($attribute);
        
        return $this;
    }
    
    /**
     * 
     * @return ArrayCollection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
    
    public function removeAttributes()
    {
        $this->attributes->clear();
        
        return $this;
    }

    public function jsonSerialize()
    {
        $product = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => $this->category
        ];
        
        $product['attributes'] = [];
        foreach ($this->attributes as $attribute) {
            $product['attributes'][] = $attribute;
        }
        
        return $product;
    }
}
