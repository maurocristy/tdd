<?php

namespace Aivo\Repositories\Rest;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Aivo\Exceptions\Repository\NotFoundException;

class UserRepository
{
    /**
     * @var ClientInterface
     */
    protected $httpClient;
    
    /**
     * @var string
     */
    protected $baseUrl;
    
    /**
     * 
     * @param ClientInterface $httpClient
     * @param string $baseUrl
     */
    public function __construct(
        ClientInterface $httpClient,
        $baseUrl
    ) {
        $this->httpClient = $httpClient;
        $this->baseUrl = $baseUrl;
    }
    
    /**
     * 
     * @param int $id
     * @return object
     * @throws NotFoundException
     */
    public function findById($id)
    {
        try {
            $response = $this->httpClient->get($this->baseUrl, [
                'query' => [
                    'id' => $id
                ]
            ]);
            
            $user = json_decode((string) $response->getBody());
        } catch (ClientException $e) {
            throw new NotFoundException($e->getMessage());
        }
            
        return $user;
    }
}
