<?php

namespace Aivo\Factories;

use Aivo\Entities\Product;
use Aivo\Entities\Attribute;

class ProductFactory
{
    /**
     * 
     * @param array $data
     * @return Product
     */
    public function create(array $data)
    {
        $product = new Product();
        $product->setName($data['name'])
            ->setDescription($data['description']);
        
        if (isset($data['attributes'])) {
            foreach ($data['attributes'] as $attrData) {
                $attribute = $this->createAttribute($attrData);
                $product->addAttribute($attribute);
                $attribute->setProduct($product);
            }
        }
        
        return $product;
    }
    
    /**
     * 
     * @param array $data
     * @return Attribute
     */
    public function createAttribute(array $data)
    {
        $attribute = new Attribute();
        $attribute->setName($data['name'])
            ->setValue($data['value']);
        
        return $attribute;
    }
    
}
