<?php

define('APP_PREFIX', "TDD_");// example prefix: "APICONFIG_"

$config['settings'] = [
    'displayErrorDetails' => true,

    'LOG_DIR'          => getenv(APP_PREFIX . 'LOG_DIR'),

    'SENTRY_ENABLED'    => filter_var(
        getenv(APP_PREFIX . 'SENTRY_ENABLED'),
        FILTER_VALIDATE_BOOLEAN,
        ['default' => true]
    ),
    'SENTRY_DSN'        => getenv(APP_PREFIX . 'SENTRY_DSN'),
    'SENTRY_OPTIONS'    => [
        'verify_ssl'    => filter_var(
            getenv(APP_PREFIX . 'SENTRY_VERIFY_SSL'),
            FILTER_VALIDATE_BOOLEAN,
            ['default' => false]
        ),
        'environment' => strtolower(getenv('ENV')),
    ],
    'DEBUG' => filter_var(
        getenv(APP_PREFIX . 'DEBUG'),
        FILTER_VALIDATE_BOOLEAN,
        ['default' => false]
    ),
    
    'apiuser.url' => getenv(APP_PREFIX . 'APIUSER_URL'),
];
