<?php

$rootPath = dirname(__DIR__);

define('VENDOR_PATH', "$rootPath/vendor");
define('APP_PATH', "$rootPath/app");
define('SRC_PATH', $rootPath . '/src');

require_once VENDOR_PATH . "/autoload.php";

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');

if (file_exists($rootPath . "/.env")) {
    $dotenv->load();
}
$dotenv->required([
    'ENV',
]);

$config = [];
require_once APP_PATH . '/config.php';

$app = new \Slim\App($config);

require_once APP_PATH . '/config/doctrine.php';

require_once APP_PATH . '/dependencies.php';

require_once APP_PATH . '/services.php';

require_once APP_PATH . '/handlers.php';

require_once APP_PATH . '/routes.php';
