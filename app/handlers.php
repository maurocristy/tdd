<?php

$container = $app->getContainer();

$container["errorHandler"] = function ($container) {
    return new Aivo\Handlers\ApiError(
        $container["logger"],
        $container["sentry"],
        $container['settings']['DEBUG']
    );
};
