<?php

/** @var \Slim\Container $container */
$container = $app->getContainer();

$container['product_factory'] = function() {
    return new Aivo\Factories\ProductFactory();
};

$container['product_service'] = function($container) {
    return new Aivo\Services\ProductService(
        $container->get('em'),
        $container->get('product_factory')
    );
};

$container['rest_user_repository'] = function($container) {
    return new Aivo\Repositories\Rest\UserRepository(
        $container->get('guzzle_client'),
        $container->get('settings')['apiuser.url']
    );
};

