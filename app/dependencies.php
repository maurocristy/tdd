<?php

/** @var \Slim\Container $container */
$container = $app->getContainer();

// parse parameters in the url
$container['foundHandler'] = function() {
    return new \Slim\Handlers\Strategies\RequestResponseArgs();
};

$container["logger"] = function ($container) {
    /** @var \Slim\Container $container */
    $logger = new \Monolog\Logger("slim");

    $formatter = new \Monolog\Formatter\LineFormatter(
        "[%datetime%] [%level_name%]: %message% %context%\n",
        null,
        true,
        true
    );

    /* Log to timestamped files */
    $rotating = new \Monolog\Handler\RotatingFileHandler(
        $container->get('settings')['LOG_DIR'],
        0,
        \Monolog\Logger::DEBUG
    );
    $rotating->setFormatter($formatter);
    $logger->pushHandler($rotating);

    return $logger;
};

$container["sentry"] = function ($container) {
    /** @var \Slim\Container $container */
    $client = null;
    if ($container->get('settings')['SENTRY_ENABLED']) {
        $client = new Raven_Client(
            $container->get('settings')['SENTRY_DSN'],
            $container->get('settings')['SENTRY_OPTIONS']
        );
    }
    return $client;
};

$container['guzzle_client'] = function () {
    return new \GuzzleHttp\Client();
};
