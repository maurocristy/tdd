<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$container = $app->getContainer();

$container['em'] = function () {

    $entitiesPath = array(SRC_PATH . '/Entities');
    $proxiesPath = APP_PATH . 'cache';
    $devMode = true;

    try {
        $redis = new Redis();
        $redis->connect(
            getenv('TDD_REDIS_SERVER'),
            getenv('TDD_REDIS_PORT')
        );
        $redis->setOption(Redis::OPT_PREFIX, 'Doctrine:');
        $cache = new \Doctrine\Common\Cache\RedisCache();
        $cache->setRedis($redis);
    } catch (\Exception $e) {
        $cache = new \Doctrine\Common\Cache\ArrayCache;
    }

    $dConfig = Setup::createAnnotationMetadataConfiguration(
        $entitiesPath,
        $devMode,
        $proxiesPath,
        $cache,
        false
    );

    $connectionOptions = array(
        'driver'   => 'pdo_mysql',
        'host'     => getenv('TDD_DB_HOST'),
        'dbname'   => getenv('TDD_DB_NAME'),
        'user'     => getenv('TDD_DB_USER'),
        'password' => getenv('TDD_DB_PASSWORD'),
        'charset'  => 'UTF8'
    );

    return EntityManager::create($connectionOptions, $dConfig);
};
