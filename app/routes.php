<?php

/** @var \Slim\Container $container */
$container = $app->getContainer();

$app->group('/api/v1', function () use ($app, $container) {
    $app->get(
        '/example',
        new \Aivo\Controllers\Example\ActionGetExample($container)
    );

    $app->get(
        '/example/{parameterOne}/{parameterTwo}',
        new \Aivo\Controllers\Example\ActionGetWithParameterExample($container)
    );

    $app->get(
        '/status/ping',
        new \Aivo\Controllers\Status\ActionPing($container)
    );

    $app->get(
        '/status/exception',
        new \Aivo\Controllers\Status\ActionException($container)
    );
    
    // Categories
    $app->group('/categories', function () use ($app, $container) {
        $app->post(
           '',
           new \Aivo\Controllers\Categories\ActionPostCategory($container)
        );
    });
    
    // Products
    $app->group('/products', function () use ($app, $container) {
        $app->post(
           '',
           new \Aivo\Controllers\Products\ActionPostProduct($container)
        );
    });
    
    // Users
    $app->group('/users', function () use ($app, $container) {
        $app->get(
           '/{id}',
           new \Aivo\Controllers\Users\ActionGetUser($container)
        );
    });
});
